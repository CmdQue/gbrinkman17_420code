PUBLIC API:

Void manualInput(bool condition)
Switch between manual and automated waveform control. 
True allows for user wave editing, false allows the Arduino to control waveform. 
Leave in manual mode for control on the fly.

Void writeNote(uint16_t Value)
Send a note value to the oscillator. 
The note value should be formatted as a standard frequency value. 
*This feature is still in development. 
    More testing needs to be done with the physical chip to ensure that it 
    reacts correctly to note value input.

Void setDigitalPots(uint8_t wiperValue)
Sets the wiper value of the digital potentiometers. 
This allows for automated control of the waveform.

void setWaveType(uint8_t value)
Set the type of wave for the system to put out. 
*Still in development.

Void systemEnable(bool condition)
Turn the entire system on and off. 
This function shuts off the input multiplexors when condition is false, 
    preventing signal from being passed to the oscillator.
